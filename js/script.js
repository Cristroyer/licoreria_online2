//Funcion volver al principio de la página
//Ver si la ventana está hasta arriba, si no, mostrar boton
$(window).scroll(function () {
  if ($(this).scrollTop() > 100) {
    $('#btnSubir').fadeIn();
  } else {
    $('#btnSubir').fadeOut();
  }
});

//Hacer click para subir
var timeOut;

function subir() {
  if (document.body.scrollTop != 0 || document.documentElement.scrollTop != 0) {
    window.scrollBy(0, -50);
    timeOut = setTimeout('subir()', 5);
  } else clearTimeout;
}



(function ($) {
  "use strict";

  $('.input').each(function () {
    $(this).on('blur', function () {
      if ($(this).val().trim() != "") {
        $(this).addClass('has-val');
      } else {
        $(this).removeClass('has-val');
      }
    })
  })




  var name = $('.validate-input input[name="nombre"]');
  var apell = $('.validate-input input[name="apellidos"]');
  var tel = $('.validate-input input[name="telefono"]');
  var email = $('.validate-input input[name="email"]');
  var message = $('.validate-input textarea[name="mensaje"]');


  $('.validate-form').on('submit', function () {
    var check = true;

    if ($(name).val().trim() == '') {
      showValidate(nombres);
      check = false;
    }

    if ($(apellidos).val().trim() == '') {
      showValidate(apellidos);
      check = false;
    }


    if ($(email).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
      showValidate(email);
      check = false;
    }

    if ($(mensaje).val().trim() == '') {
      showValidate(mensaje);
      check = false;
    }


    if ($(telefono).val().trim() == '') {
      showValidate(telefono);
      check = false;
    }

    return check;
  });


  $('.validate-form .input2').each(function () {
    $(this).focus(function () {
      hideValidate(this);
    });
  });

  function showValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).addClass('alert-validate');
  }

  function hideValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).removeClass('alert-validate');
  }



})(jQuery);